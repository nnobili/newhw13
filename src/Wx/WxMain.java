package Wx;

import java.io.IOException;

//import ch.makery.wx.view.WxController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class WxMain extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Wx - Nicolas Nobili");

        showWxView();
    }

    public void showWxView() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(WxMain.class.getResource("WxView.fxml"));
            
            AnchorPane WxView = (AnchorPane) loader.load();
            

            // Show the scene containing the root layout.
            Scene scene = new Scene(WxView);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
